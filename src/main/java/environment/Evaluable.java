package environment;

import java.util.List;

/**
 * The functional interface used by the environment
 * to store system defined and user defined data
 * structures and variables. 
 *
 * @author Max Jonsson
 * @version 2020-01-17
 */
@FunctionalInterface
public interface Evaluable {
    public String evaluate(List<String> args);
}
