package environment;

import java.util.HashMap;

/**
 * The environment for the interpreter. This is where
 * general purpose operations are stored and also where
 * user defined methods and variables are added in
 * run time.
 * 
 * @author Max Jonsson
 * @version 2020-01-17
 */
public class Environment {
    
	private static final HashMap<String, Evaluable> dict = new HashMap<>();
	
	public static void add(String key, Evaluable evaluable) {
		dict.put(key, evaluable);
	}
	
	public static Evaluable get(String key) {
		return dict.get(key);
	}
	
	public static void setupEnvironment() {
		Environment.add("+", args -> {
			return args
					.stream()
					.map(s -> Integer.parseInt(s))
					.reduce(0, (acc, elt) -> acc + elt)
					.toString();
		});
		
		Environment.add("-", args -> {
			int first = Integer.parseInt(args.remove(0));
			return args
					.stream()
					.map(s -> Integer.parseInt(s))
					.reduce(first, (acc, elt) -> acc - elt)
					.toString();
		});
		
		Environment.add("*", args -> {
			return args
					.stream()
					.map(s -> Integer.parseInt(s))
					.reduce(1, (acc, elt) -> acc * elt)
					.toString();
		});
		
		Environment.add("/", args -> {
			int first = Integer.parseInt(args.remove(0));
			return args
					.stream()
					.map(s -> Integer.parseInt(s))
					.reduce(first, (acc, elt) -> acc / elt)
					.toString();
		});
		
		Environment.add("T", args -> "T");
		
		Environment.add("NIL", args -> "NIL");
		
		Environment.add("=", args -> {
			String first = args.remove(0);
			String second = args.remove(0);
			
			return first.equals(second) ? "T" : "NIL";
		});
		
		Environment.add("/=", args -> {
			String first = args.remove(0);
			String second = args.remove(0);
			
			return first.equals(second) ? "NIL" : "T";
		});
		
		Environment.add("<", args -> {
			int first = Integer.parseInt(args.remove(0));
			int second = Integer.parseInt(args.remove(0));
			
			return first < second ? "T" : "NIL";
		});
		
		Environment.add(">", args -> {
			int first = Integer.parseInt(args.remove(0));
			int second = Integer.parseInt(args.remove(0));
			
			return first > second ? "T" : "NIL";
		});
		
		Environment.add("<=", args -> {
			int first = Integer.parseInt(args.remove(0));
			int second = Integer.parseInt(args.remove(0));
			
			return first <= second ? "T" : "NIL";
		});
		
		Environment.add(">=", args -> {
			int first = Integer.parseInt(args.remove(0));
			int second = Integer.parseInt(args.remove(0));
			
			return first >= second ? "T" : "NIL";
		});
	}
	
}
