package com.mycompany.lisp;

import environment.Environment;
import interpreter.Interpreter;
import java.util.List;

/**
 * Launches the interpreter and sends it instructions
 * to evaluate.
 *
 * @author Max Jonsson
 * @version 2020-01-17
 */
public class Launcher {
	
	public static void main(String[] args) {
		Environment.setupEnvironment();
		
		Interpreter interpreter = new Interpreter();
		interpreter.interpret("(define r 10)");
		List<String> result = interpreter.interpret("(if (= T NIL) T NIL)");
		
		System.out.println(result);
	}
	
}
