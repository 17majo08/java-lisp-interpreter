package structure;

/**
 * The parts of a Lisp Expression that are numeric values.
 * 
 * @author Max Jonsson
 * @version 2020-01-17
 */
public class Number extends Atom {
    
	private final int number;
	
	public Number(int num) {
		this.number = num;
	}

	@Override
	public String evaluate() {
		return Integer.toString(number);
	}

	@Override
	public String toString() {
		return Integer.toString(number);
	}
	
}
