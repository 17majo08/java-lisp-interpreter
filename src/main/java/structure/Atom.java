package structure;

/**
 * The definition of an Atom is everything that can be a part
 * of a Lisp expression, that is Numbers, Symbols and other
 * Expressions. Hence, this module works as the abstract
 * superclass that makes it possible to treat each part of an
 * expression under same labels.
 * 
 * @author Max Jonsson
 * @version 2020-01-17
 */
public abstract class Atom {
	public abstract String evaluate();
	
	@Override
	public abstract String toString();
}
