package structure;

import environment.Environment;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This is the Lisp expression that consist of one operation and
 * a list of arguments to perform the operation on. The arguments
 * are of type Atom, hence they can either be Symbols, Numbers or
 * other Lisp Expressions.
 *
 * @author Max Jonsson
 * @version 2020-01-17
 */
public class Expression extends Atom {
	
	private final String operation;
	private final List<Atom> arguments;
	
	public Expression(String op, List<Atom> args) {
		this.operation = op;
		this.arguments = args;
	}
	
	public String getOperation() {
		return this.operation;
	}
	
	public List<Atom> getArguments() {
		return this.arguments;
	}
	
	@Override
	public String evaluate() {
		switch (operation) {
			case "if":
				String condition = arguments.remove(0).evaluate();
				
				if (condition.equals("NIL"))
					arguments.remove(0);
				
				return arguments.get(0).evaluate();
			case "define":
				String variable = arguments.remove(0).toString();
				String value = arguments.remove(0).toString();
				Environment.add(variable, a -> value);
				
				return variable;
			default:
				List<String> evaluatedArgs = arguments.stream()
						.map(atom -> atom.evaluate())
						.collect(Collectors.toList());
				
				return Environment.get(operation).evaluate(evaluatedArgs);
		}
		
	}
	
	@Override
	public String toString() {
		String args = arguments.stream()
				.map(atom -> atom.toString())
				.collect(Collectors.joining(" "));
		
		return String.format("(%s %s)", operation, args);
	}
	
}
