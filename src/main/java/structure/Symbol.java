package structure;

import environment.Environment;

/**
 * The parts of a Lisp Expression that are not numeric values. Symbols
 * can both be system defined and user defined and in order to get its
 * numeric value the Environment module is used.
 * 
 * @author Max Jonsson
 * @version 2020-01-17
 */
public class Symbol extends Atom {
    
	private final String symbol;
	
	public Symbol(String symb) {
		this.symbol = symb;
	}

	@Override
	public String evaluate() {
		return Environment.get(symbol).evaluate(null);
	}

	@Override
	public String toString() {
		return symbol;
	}
	
}
