package interpreter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import stack.Stack;
import structure.Atom;
import structure.Expression;
import structure.Number;
import structure.Symbol;

/**
 * The interpreter module converts the input string to a single
 * value that represents the result of the given operation. It
 * does so by sending it through a process of operations defined
 * by the methods tokenize, read and eval.
 * 
 * @author Max Jonsson
 * @version 2019-11-02
 */
public class Interpreter {
	
	public List<String> interpret(String exp) {
		return eval(read(tokenize(exp)));
	}
	
	private List<String> tokenize(String exp) {
		String[] tokens = exp
			.replaceAll("[(]", " ( ")
			.replaceAll("[)]", " ) ")
			.replaceAll(" +", " ")
			.strip()
			.split("\\s");
		
		return new ArrayList<>(Arrays.asList(tokens));
	}
	
	private List<Atom> read(List<String> tokens) {
		List<Atom> atoms = new ArrayList<>();
		
		for (int i = 0; i < tokens.size(); i++) {
			String token = tokens.get(i);
			
			if (token.equals("(")) {
				Stack<String> stack = new Stack<>();
				stack.push(token);
				
				i++;
				String operation = tokens.get(i);
				List<String> atomTokens = new ArrayList<>();
				
				do {					
					i++;
					String atom = tokens.get(i);
					
					switch (atom) {
						case "(":
							stack.push(atom);
							atomTokens.add(atom);
							break;
						case ")":
							stack.pop();
							if (!stack.isEmpty())
								atomTokens.add(atom);
							break;
						default:
							atomTokens.add(atom);
							break;
					}
				} while (!stack.isEmpty());
				
				atoms.add(new Expression(operation, read(atomTokens)));
			} else {
				if (token.matches("-?(0|[1-9]\\d*)"))
					atoms.add(new Number(Integer.parseInt(token)));
				else
					atoms.add(new Symbol(token));
			}
		}
		
		return atoms;
	}
	
	private List<String> eval(List<Atom> atoms) {
		return atoms.stream()
				.map(atom -> atom.evaluate())
				.collect(Collectors.toList());
	}
	
}
